#!/usr/bin/python
# Parse/format certificates exported from database
# Author: Maer Melo
#         salalbr@gmail.com
##################################################

import sys, os

def usage():
	print 'Usage: proc_zcert.py <csv export>'
	return

if (len(sys.argv) != 2):
	usage()
	sys.exit(2)

filename = sys.argv[1]

cert = 0
new_file = 'T'
lines = [line.strip() for line in open(filename)]
pos = 0

#print lines

for i in range(len(lines)):

	#print len(lines[i])
	
	if new_file == 'T':
		cert += 1
		outputfile = open('cert' + str(cert) + '.test', 'w')
		outputfile.write('-----BEGIN CERTIFICATE-----\n')
		new_file = 'F'
		
	
	for j in range (len(lines[i])):
		if pos > 63:
			outputfile.write('\n')
			pos = 0
		outputfile.write(lines[i][j])
		pos += 1
	if len(lines[i]) < 76:
		outputfile.write('\n-----END CERTIFICATE-----\n')
		new_file = 'T'
		pos = 0

print len(lines[0])
print lines[0]
print lines[1]
